const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

const Product = require("../models/Product");

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		};
	});
};

module.exports.loginUser = (reqBody) => {

	// We use the "findOne" method instead of the "find" method which return all records that match the search criteria
	// The "findOne" method returns the first record in the collection that matches the search criteria
	return User.findOne({email : reqBody.email}).then(result => {

		// User does not exist
		if(result == null){
			return false;
		
		// User exist
		} else {

			// The "compareScync" method is used to compate a non encrypted password from the login form to the excrypted password retrived from the database and it returns "true" or "false" value depending on the result
			// A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect){
				
				// Generate an access token
				return { access : auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

module.exports.getProfile = (data) => {
	
	return User.findById(data.userId).then(result => {
		
		// Changes the value of the user's password to an empty string when returned to the frontend
		result.password = "";
		return result;
	});
};

	// Async await will be used in purchasing by the user because we will need to update 2 separate documents when purchasing
module.exports.order = async (data) => { 
	// "async" is placed in the line 77 to declate that this is an async expression. 
	// Given that this is an async expression, "await" keyword should be permitted within the body

		// An async expression, is an expression that returns a promise.

	let isUserUpdated = await User.findById(data.userId).then(user => {
		// "await" is written in line 83, so that, this tells our code to wait for the promise to resolve, in this case for our user order to be "push" in our database.


		// Adds the productId in the user's orders array
		user.orderedProduct.push({ productId : data.productId });

		// Saves the updated user information in the database
		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});


	let isProductUpdated = await Product.findById(data.productId).then(product => {
		// "await" is written in line 98, so that, this tells our code to wait for the promise to resolve, in this case for our product buyer to be "push" in our database.

		// Adds the userId in the product's order array
		product.userOrders.push({ userId : data.userId });

		// Saves the updated product information in the database
		return product.save().then((product, error) => {
			if(error){
				return false
			} else {
				return true
			};
		});
	});

	
	// To check if we are successful in fulfilling both promise, we will utilize the if statement and the double ampersand as representation of logical "AND"
	if(isUserUpdated && isProductUpdated){
		return true;
	} else {
		return false;
	};

};

	